/* eslint-disable no-ex-assign */
const logger = require('../../services/logger')(module);
const db = require('../../models');

const { user: User } = db;

async function duplicateCredentials(req, res, next) {
  try {
    const { username, email } = req.body;

    const existingUser = await User.findOne({ username });
    if (existingUser) {
      res.status(400);
      throw new Error('Username is already in use');
    }
    const existingEmail = await User.findOne({ email });
    if (existingEmail) {
      res.status(400);
      throw new Error('Email is already in use');
    }
    return next();
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

function validCredentials(req, res, next) {
  try {
    const { username, email, password } = req.body;

    if (!(username && email && password)) {
      res.status(400);
      throw new Error('Missing credentials');
    }
    if (!email.match(/^\S+@\S+\.\S+$/)) {
      res.status(400);
      throw new Error('Email doesn\'t appear to be valid');
    }
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }

  return next();
}

module.exports = {
  duplicateCredentials,
  validCredentials,
};
