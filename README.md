# Установка и запуск

Проект запускался с MongoDB 5.0 в Docker и Node.js 16

1. Клонировать этот репозиторий
2. Запустить MongoDB и восстановить дамп

   ```
   cd test-api-js-backend
   docker run --rm -d -p 27017:27017 --name test-api-mongo mongo:5.0
   docker cp db_dump test-api-mongo:/
   docker exec -i test-api-mongo /usr/bin/mongorestore --db test_db /db_dump/test_db
   ```
3. Запуск и тесты

   ```
   npm install
   npm run start
   npm run test 
   ```

# API

**Авторизация**

<details>
   <summary>Формат</summary>
   <pre>
      {
         "username": "String",
         "password": "String",
         "email": "String"
      }
   </pre>
</details>

* `POST  /auth/register`   Регистрация нового пользователя, JSON body
* `POST  /auth/login`      Войти как пользователь, JSON body


**Компании**

 <details>
   <summary>Формат</summary>
   <pre>
      {
         "contactId": "String",
         "name": "String",
         "shortName": "String",
         "businessEntity": "String",
         "address": "String",
         "contract": {
            "no": "Number",
            "issue_date": "Date"
         },
         "type": [ "String" ],
         "status": "String"
      }
   </pre>
</details>

* `POST   /companies/`     Добавить новую компанию, JSON body
* `DELETE /companies/:id`  Удалить компанию, id param
* `GET    /companies/:id`  Показать компанию, id param
* `PATCH  /companies/:id`  Обновить компанию, id param
* `GET    /companies/list` Получить список компаний,
   * Пагинация: `limit=20`, `skip=10`
   * Сортировка: `sortBy=createdAt:desc`, доступные поля: `name, createdAt`, порядок `desc, asc`
   * Фильтрация: `filterBy=status:active`, доступные поля: `status: [active], type: [agent, contractor]`
   * Несколько параметров: `?sortBy=createdAt:desc&sortBy=name:asc`

**Контакты**

<details>
   <summary>Формат</summary>
   <pre>
      {
         "lastname": "String",
         "firstname": "String",
         "patronymic": "String",
         "phone": "String",
         "email": "String"
      }
   </pre>
</details>

* `POST   /contacts/`     Добавить новый контакт, JSON body
* `DELETE /contacts/:id`  Удалить контакт, id param
* `GET    /contacts/:id`  Показать контакт, id param
* `PATCH  /contacts/:id`  Обновить контакт, id param
