const request = require('supertest');
const assert = require('assert');
const { v4: uuidv4 } = require('uuid');
const app = require('../app');

const _company = {
  contactId: '12',
  name: 'ООО Фирма «Перспективные захоронения»',
  shortName: 'Перспективные захоронения',
  businessEntity: 'ООО',
  address: 'Ул. 5',
  contract: {
    no: '12345',
    issue_date: '2015-03-12T00:00:00Z',
  },
  type: ['agent', 'contractor'],
  status: 'active',
};
const _contact = {
  id: '16',
  lastname: 'Григорьев',
  firstname: 'Сергей',
  patronymic: 'Петрович',
  phone: '79162165588',
  email: 'grigoriev@funeral.com',
  createdAt: '2020-11-21T08:03:26.589Z',
  updatedAt: '2020-11-23T09:30:00Z',
};

describe('Task API Routes', () => {
  let _jwtToken = '';
  // Auth
  describe('Test /auth route', () => {
    const [password, username, email] = uuidv4().split('-');
    it('Register a new user', (done) => {
      request(app)
        .post('/auth/register')
        .send({
          username, password, email: `${email}@example.com`,
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect('Authorization', /Bearer/)
        .expect(200)
        .end((err, res) => {
          assert.ok(res.body._id);
          assert.ok(res.body.password);
          done(err);
        });
    });
    it('Login as an existing user', (done) => {
      request(app)
        .post('/auth/login')
        .send({
          username, password,
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect('Authorization', /Bearer/)
        .expect(200)
        .end((err, res) => {
          _jwtToken = res.body.accessToken;
          assert.ok(res.body.id);
          assert.ok(res.body.accessToken);
          done(err);
        });
    });
  });

  // Companies
  describe('Test /companies route', () => {
    let id = '62d3efa6b0ace550c67e67d0';
    it('Add a new company', (done) => {
      request(app)
        .post('/companies')
        .auth(_jwtToken, { type: 'bearer' })
        .send(_company)
        .set('Accept', 'application/json')
        .expect(200)
        .end((err, res) => {
          assert.ok(res.body._id);
          assert(res.body.name, _company.name);
          id = res.body._id;
          done(err);
        });
    });
    it('Get an existing company', (done) => {
      request(app)
        .get(`/companies/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.name, _company.name);
          done(err);
        });
    });
    it('Update an existing company', (done) => {
      const companyUpd = { status: 'inactive' };
      request(app)
        .patch(`/companies/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .send(companyUpd)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body._id, id);
          assert(res.body.status, companyUpd.status);
          done(err);
        });
    });
    it('Delete an existing company', (done) => {
      request(app)
        .delete(`/companies/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .expect(200)
        .end((err) => {
          done(err);
        });
    });
    const _skip = 2;
    const _limit = 5;
    it(`List all active companies, skipping first ${_skip}, limited to ${_limit}`, (done) => {
      const q = `filterBy=status:active&skip=${_skip}&limit=${_limit}`;
      request(app)
        .get(`/companies/list?${q}`)
        .auth(_jwtToken, { type: 'bearer' })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.ok(Array.isArray(res.body));
          assert.equal(res.body.length, _limit);
          assert.ok(res.body.every((x) => x.status === 'active'));
          done(err);
        });
    });
  });

  // Contacts
  describe('Test /contacts route', () => {
    let id = '62d3fafcf0091590c39fdd09';
    it('Add a new contact', (done) => {
      request(app)
        .post('/contacts')
        .auth(_jwtToken, { type: 'bearer' })
        .send(_contact)
        .set('Accept', 'application/json')
        .expect(200)
        .end((err, res) => {
          assert(res.body.email, _contact.email);
          assert(res.body.firstname, _contact.firstname);
          id = res.body._id;
          done(err);
        });
    });
    it('Get an existing contact', (done) => {
      request(app)
        .get(`/contacts/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert(res.body.email, _contact.email);
          assert(res.body.firstname, _contact.firstname);
          done(err);
        });
    });
    it('Update an existing contact', (done) => {
      const contactUpd = { phone: 5544332211 };
      request(app)
        .patch(`/contacts/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .send(contactUpd)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.ok(res.body._id, id);
          assert(res.body.phone, contactUpd.phone);
          done(err);
        });
    });
    it('Delete an existing contact', (done) => {
      request(app)
        .delete(`/contacts/${id}`)
        .auth(_jwtToken, { type: 'bearer' })
        .expect(200)
        .end((err) => {
          done(err);
        });
    });
  });
});
