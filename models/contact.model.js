const mongoose = require('mongoose');

const Contact = mongoose.model(
  'Contact',
  new mongoose.Schema(
    {
      lastname: { type: String, required: true },
      firstname: { type: String, required: true },
      patronymic: String,
      phone: String,
      email: { type: String, required: true },
    },
    { timestamps: true },
  ),
);

module.exports = Contact;
