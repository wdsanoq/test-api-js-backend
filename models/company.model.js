const mongoose = require('mongoose');

const Company = mongoose.model(
  'Company',
  new mongoose.Schema(
    {
      contactId: String,
      name: String,
      shortName: String,
      businessEntity: String,
      address: String,
      contract: {
        no: Number,
        issue_date: Date,
      },
      type: [String],
      status: String,
    },
    { timestamps: true },
  ),
);

module.exports = Company;
