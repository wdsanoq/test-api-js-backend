const mongoose = require('mongoose');

const db = {};
db.mongoose = mongoose;
db.user = require('./user.model');
db.contact = require('./contact.model');
db.company = require('./company.model');

mongoose.Promise = global.Promise;
module.exports = db;
