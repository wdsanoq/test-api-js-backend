const mongoose = require('mongoose');

const User = mongoose.model(
  'User',
  new mongoose.Schema({
    email: {
      type: String,
      unique: true,
      required: true,
      lowercase: true,
      minLength: 6,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
      minLength: 6,
    },
  }),
);

module.exports = User;
