/* eslint-disable no-ex-assign */
const config = require('../config.json');
const logger = require('../services/logger')(module);
const Company = require('../models/company.model');

module.exports = {
  add,
  get,
  update,
  del,
  list,
};

/*
const company = {
  id: config.company_id,
  contactId: config.contact_id,
  name: 'ООО Фирма «Перспективные захоронения»',
  shortName: 'Перспективные захоронения',
  businessEntity: 'ООО',
  address: '123 St',
  contract: {
    no: '12345',
    issue_date: '2015-03-12T00:00:00Z',
  },
  type: ['agent', 'contractor'],
  status: 'active',
  createdAt: '2020-11-21T08:03:00Z',
  updatedAt: '2020-11-23T09:30:00Z',
};
*/

async function add(req, res) {
  try {
    const company = await Company.create({ ...req.body });
    return res.status(200).json(company);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

function _getListFilter(req, res) {
  const allowedFilters = { status: ['active'], type: ['agent', 'contractor'] };
  let filter = {};
  if (req.query.filterBy) {
    let { filterBy } = req.query;
    filterBy = typeof filterBy === 'string' ? [filterBy] : filterBy;
    filterBy.forEach((ft) => {
      const parts = ft.split(':');
      const currentFilter = allowedFilters[parts[0]];
      if (!currentFilter) {
        res.status(400);
        throw new Error(`Invalid filterBy key ${parts[0]}`);
      }
      if (currentFilter.indexOf(parts[1]) === -1) {
        res.status(400);
        throw new Error(`Invalid filterBy value "${parts[1]}" for key "${parts[0]}"`);
      }
      filter = { ...filter, [parts[0]]: parts[1] };
    });
  }
  return filter;
}
function _getListSort(req, res) {
  const allowedSortKeys = ['name', 'createdAt'];
  const sort = {};
  if (req.query.sortBy) {
    let { sortBy } = req.query;
    sortBy = typeof sortBy === 'string' ? [sortBy] : sortBy;
    sortBy.forEach((s) => {
      const parts = s.split(':');
      if (allowedSortKeys.indexOf(parts[0]) === -1) {
        res.status(400);
        throw new Error(`Invalid sortBy key "${parts[0]}"`);
      }
      sort[parts[0]] = parts[1] === 'desc' ? -1 : 1;
    });
  }
  return sort;
}
async function list(req, res) {
  try {
    const filter = _getListFilter(req, res);
    const sort = _getListSort(req, res);
    let limit = 10;
    if (req.query.limit) {
      const iLimit = parseInt(req.query.limit, 10);
      if (!Number.isNaN(iLimit) && iLimit > 0) {
        limit = Math.min(100, iLimit);
      } else {
        res.status(400);
        throw new Error('Invalid limit parameter');
      }
    }
    let skip = 0;
    if (req.query.skip) {
      const iSkip = parseInt(req.query.skip, 10);
      if (!Number.isNaN(iSkip) && iSkip > 0) {
        skip = iSkip;
      } else {
        res.status(400);
        throw new Error('Invalid skip parameter');
      }
    }

    const companies = await Company
      .find(filter)
      .sort(sort)
      .setOptions({ limit, skip });
    return res.status(200).json({ companies });
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function get(req, res) {
  const URL = _getCurrentURL(req);

  try {
    const { id } = req.params;
    const result = await Company.findById(id);
    result.photos = [{
      name: '0b8fc462dcabf7610a91.png',
      filepath: `${URL}0b8fc462dcabf7610a91.png`,
      thumbpath: `${URL}0b8fc462dcabf7610a91_160x160.png`,
    }];

    return res.status(200).json(result);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function update(req, res) {
  try {
    const { id } = req.params;
    const company = await Company.findByIdAndUpdate(
      { _id: id },
      { ...req.body },
      { new: true },
    );
    return res.status(200).json(company);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function del(req, res) {
  try {
    const { id } = req.params;
    await Company.findByIdAndDelete(id);
    return res.status(200).end();
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

function _getCurrentURL(req) {
  const { port } = config;
  return `${req.protocol}://${req.hostname}${port === '80' || port === '443' ? '' : `:${port}`}/`;
}
