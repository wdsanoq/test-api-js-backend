/* eslint-disable no-ex-assign */
const Contact = require('../models/contact.model');
const logger = require('../services/logger')(module);

module.exports = {
  add,
  get,
  update,
  del,
};

/*
const contact = {
  id: config.contact_id,
  lastname: 'Григорьев',
  firstname: 'Сергей',
  patronymic: 'Петрович',
  phone: '79162165588',
  email: 'grigoriev@funeral.com',
  createdAt: '2020-11-21T08:03:26.589Z',
  updatedAt: '2020-11-23T09:30:00Z',
};
*/

async function get(req, res) {
  try {
    const { id } = req.params;
    const contact = await Contact.findById(id);
    return res.status(200).json(contact);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function add(req, res) {
  try {
    const contact = await Contact.create({ ...req.body });
    return res.status(200).json(contact);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function del(req, res) {
  try {
    const { id } = req.params;
    await Contact.findByIdAndDelete(id);
    return res.status(200).end();
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}

async function update(req, res) {
  try {
    const { id } = req.params;
    const contact = await Contact.findByIdAndUpdate(
      { _id: id },
      { ...req.body },
      { new: true },
    );
    return res.status(200).json(contact);
  } catch (error) {
    error = error.toString();
    logger.error(error);
    return res.status(400).json({ error });
  }
}
