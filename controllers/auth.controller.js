const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../models');
const logger = require('../services/logger')(module);
const config = require('../config.json');

const User = db.user;
const saltRounds = 10;

const _getToken = (id) => jwt.sign({ id }, config.jwt_secret, { expiresIn: config.jwt_ttl });

async function register(req, res) {
  const { username, email, password: plainPassword } = req.body;
  const hashedPassword = await bcrypt.hash(plainPassword, saltRounds);

  const user = new User({
    username, email, password: hashedPassword,
  });
  user.save((error) => {
    if (error) {
      error = error.toString();
      logger.error(error);
      return res.status(400).json({ error });
    }
    res.header('Authorization', `Bearer ${_getToken(user.id)}`);
    return res.status(200).json(user);
  });
}

function login(req, res) {
  const { username } = req.body;
  User.findOne({ username })
    .exec(async (error, user) => {
      if (error) {
        error = error.toString();
        logger.error(error);
        return res.status(400).json({ error });
      }
      if (!user) {
        return res.status(400).json({
          error: 'User doesn\'t exist',
        });
      }
      const isValidPassword = await bcrypt.compare(req.body.password, user.password);
      if (!isValidPassword) {
        return res.status(400).json({
          error: 'Invalid credentials',
        });
      }
      res.header('Authorization', `Bearer ${_getToken(user.id)}`);
      return res.status(200).send({
        id: user._id,
        username: user.username,
        email: user.email,
        accessToken: _getToken(user.id),
      });
    });
}

module.exports = {
  register,
  login,
};
